#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <time.h>
#include <stdbool.h>

//an individual in the population
struct ind{
	int *val;  	//bit-string as a 1-d array
	int fitness;
	struct ind *next;
};

int umda(int lmbda, int mu, int n, int optimal_fitness, int id, int max_iters);
struct ind *create_ind(int *val, int fitness);
struct ind *add_ind(struct ind *head, struct ind *newind);
struct ind *sort_pop(struct ind *x);
struct ind *find_middle(struct ind *x);
struct ind *merge(struct ind *x, struct ind *y);
int evaluator(int *val, int n);
int *array_sum(int *arr1, int *arr2, int n);
void model_printer(int iter, int best, float *p, int n);
void free_ind(struct ind *p);
void print_ind(int *val, int n);
void print_p(float *p, int n);
int max(int a, int b);
bool xLessThanY(int *x, int *y);

//umda implementation
int umda(int lmbda, int mu, int n, int optimal_fitness, int id, int max_iters){
	float p[n];  	 //model representation ~ array
	int i,j;

	// random number generator
	const gsl_rng_type * T;
  	gsl_rng * r;
	gsl_rng_env_setup();
  	T = gsl_rng_default;
 	r = gsl_rng_alloc (T);
 	gsl_rng_set(r, (unsigned long) n*id*time(NULL));

	for (i=0; i<n; i++){    //initial model
		p[i] = 0.5;
	}
	struct ind *head = NULL;
	for (i=0; i<lmbda; i++){  //sample initial population
		int *val = (int *)malloc(n*sizeof(int));
		for (j=0; j<n; j++){
			float u = gsl_rng_uniform (r);
			val[j] = (u <= p[j])?1:0;
		}
		int fitness = evaluator(val, n);
		head = add_ind(head, create_ind(val,fitness));
	}
	int iterations = 0;
	struct ind *curr = NULL;
	while(1){

		int ups = 0;   // number of bits at upper border
		int lows = 0;  // number of bits at lower border
		int inbws = 0;  // number of bits inbetween

		int cLevel = 0;  //current level
		int upsAfterCLevel = 0;  //number of bits at upper borders after cLevel
		int lowsAfterCLevel = 0; // ... at lower borders ...
		int inbwsAfterCLevel = 0; // ... inbetween ...

		iterations++;

		if (iterations > max_iters){
			printf("iters exceeded %d ", max_iters);
			break;
		}

		head = sort_pop(head);  //sort population

		int t = mu;
		int * arr = head->val;
		int best_fit = head->fitness;	//best fitness value

		/*
		if (print_model){
			model_printer(iterations, best_fit, p, n);
		}
		*/

		if (best_fit == optimal_fitness){   //terminate?
			break;
		}

		for (curr=head->next; curr != NULL && t>1; curr=curr->next,t--){
			arr = array_sum(arr,curr->val,n);
		}

		//print the sorted population
		/*
		for (curr=head; curr != NULL; curr = curr->next){
			int indx = 0;
			for (; indx < n; indx++){
				printf("%d", curr->val[indx]);
			}
			printf("\n");
		}
		*/

		// calculate the current level
		i = 0;
		while (arr[i] == mu){
			cLevel++;
			i++;
		}

		for (i=0; i<n; i++){
			int x_i = arr[i];
			// updating the model
			if (x_i == 0){
				lows++;
				p[i] = 1.0/n;
			} else if (x_i == mu){
				ups++;
				p[i] = 1-(1.0/n);
			} else {
				inbws++;
				p[i] = ((float)x_i)/mu;
			}
			// consider bits from the current cLevel
			if (i >= cLevel){
				if (x_i == 0) lowsAfterCLevel++;
				else if (x_i == mu) upsAfterCLevel++;
				else inbwsAfterCLevel++;
			}
		}

		//printf("%d \t %d \t %d \t %d \t %d \t %d \t %d \t %d \n", iterations, cLevel, ups, lows, inbws, upsAfterCLevel, lowsAfterCLevel, inbwsAfterCLevel );

		curr = head;    //reset curr
		int x;
		while (curr != NULL){  //sample new population
			for (x=0; x<n; x++){
				float u = gsl_rng_uniform (r);
				curr->val[x] = (u <= p[x])?1:0;
			}
			curr->fitness = evaluator(curr->val, n);
			curr = curr->next;
		}
	}
	free_ind(head);
	return (lmbda*iterations);
}

//print the current model
void model_printer(int iter, int best, float *p, int n){
	int i;
	printf("%d \t %d \t", iter, best);
	for (i=0; i<n; i++){
		printf("%f \t", p[i]);
	}
	printf("\n");
}

//print an individual
void print_ind(int *val, int n){
	int i;
	for (i=0;i<n;i++){
		printf("%d ",val[i]);
	}
	printf("\n");
}

//print the current probability model
void print_p(float *p, int n){
	int i;
	printf("Prob. model:\n");
	for (i=0;i<n;i++){
		printf("%.2f ",p[i]);
	}
	printf("\n");
}

//evaluate binval
int evaluator(int *val, int n){
	int i=0;   //index
	int lons = 0;
	for (; i<n; i++){
	    if (val[i] == 1) //check for complete blocks
				lons++;
	    else
				break;
	}
	return lons;
}


int *array_sum(int *arr1, int *arr2, int n){
	int i;
	for (i=0;i<n;i++){
		arr1[i] = arr1[i] + arr2[i];
	}
	return arr1;
}

//create a dictionary
struct ind *create_ind(int *val, int fitness){
	struct ind *p = (struct ind *)malloc(sizeof(struct ind));
	if (p == NULL){
		return NULL;
	}
	p->val = val;
	p->fitness = fitness;
	p->next = NULL;
	return p;
}

//add a new dict into the current dict
struct ind *add_ind(struct ind *head, struct ind *new){
	if (head == NULL){
		return new;
	}
	if (new != NULL){
		new->next = head;
		head = new;
	}
	return head;
}

//free the memory
void free_ind(struct ind *head){
	struct ind *tmp;
	while (head != NULL){
		tmp = head;
		head = head->next;
		free(tmp->val);
		free(tmp);
	}
}

//sort a dictionary
struct ind *sort_pop(struct ind *x){

	struct ind *y = NULL;
	struct ind *m = NULL;

	if ( x == NULL || x->next == NULL ){
		return x;
	}
	m = find_middle(x);
	y = m->next;
	m->next = NULL;
	return merge(sort_pop(x), sort_pop(y));
}

//find the middle elem in a dictionary
struct ind *find_middle(struct ind *x){

	struct ind *slow = x;
	struct ind *fast = x;

	while( fast->next != NULL && fast->next->next != NULL ){
		slow = slow->next;
		fast = fast->next->next;
	}
	return slow;
}

//merge two dicts
struct ind *merge(struct ind *x, struct ind *y){

	struct ind *tmp  = NULL;
	struct ind *head = NULL;
	struct ind *curr = NULL;

	if( x == NULL ){
		head = y;
	}else if( y == NULL ){
		head = x;
	}else {
		while( x != NULL && y != NULL ) {

			// Swap x and y if x is not largest.
			//if( x->fitness < y->fitness ) {
			if (xLessThanY(x -> val, y -> val)){
				tmp = y;
				y   = x;
				x   = tmp;
			}

			if( head == NULL ) { // First element?
				head = x;
				curr = x;
			} else {
				curr->next = x;
				curr = curr->next;
			}
			x = x->next;
		}

		// Either x or y is empty.
		if( x != NULL ){
			curr->next = x;
		}else if( y != NULL ){
			curr->next = y;
		}
	}
	return head;
}

bool xLessThanY(int * x, int * y){
	if (x == NULL){  //check for null arrays
		return false;
	}
	if (x[0] < y[0]){
		return true;
	} else if (x[0] > y[0]){
		return false;
	} else {
		return xLessThanY(&x[1], &y[1]);
	}
}

//find the max between two ints
int max(int a, int b){
	return (a>=b)?a:b;
}

int main(int argc, char **argv){

	if (argc != 7){
		printf("USAGE: %s  n id config(0-1) power gamma max_iters\n", argv[0]);
		exit(1);
	}

	int n = atoi(argv[1]); 		 //problem size n
	int id = atoi(argv[2]); 	 //job id
	int config = atoi(argv[3]);  //config
	double power = atof(argv[4]);
	double gamma = atof(argv[5]);
	int max_iters = atoi(argv[6]);

	//printf("Config: %d %d %d %f %f \n",n, id, config, power, gamma);
	//bool print_model = (atoi(argv[6])!=0);
	//int gap = atoi(argv[6]);  //gap

	int lmbda;
	int mu;

	if (config == 0){ //small mu
			mu = (int) 5*(log10(n)/log10(2));
	}

	if (config == 1){  //large mu
			mu = (int) 10 * pow(n, power); //mu = sqrt(n)*log2(n)
	}

	lmbda = (float) mu / gamma;

	int optimal_fitness = n;

	int res = umda(lmbda, mu, n, optimal_fitness, id, max_iters);
	//if (!print_model) {
	printf("%d \t %d \t %d \n", n, id, res);
	return(0);
}

small.mu = read.table(file = "pbil_los_low_small", header = FALSE)
medium.mu = read.table(file = "pbil_los_low_medium", header = FALSE)
large.mu = read.table(file = "pbil_los_low_large", header = FALSE)

setEPS()
postscript(file="pbil_los_low.pdf", width = 16.0, height = 3)
par(mfrow=c(1,3))
require(plotrix)

par(mar=c(5, 5, 4, 2) + 0.1)
matplot(small.mu$V1, cbind(small.mu$V2, small.mu$V3), type = "p", 
        pch = c(1,16), col = c(1,2), main="Small population size", 
        tck=0.03, ylim = c(0,100),xlab = "Iterations", ylab = "Bit index",
        cex.lab=2.0, cex.axis=2.0, cex.main=2)
abline(46.76,0, col=4, lty=3, lwd=2)
abline(68.96,0, lty=3)
abline(87.11,0, lty=3, col=4, lwd=2)
legend(0, 100,legend = c("Z", "Z star"), pch = c(1,16), col = c(1,2), cex = 1.5)

par(mar=c(5, 5, 4, 2) + 0.1)
matplot(medium.mu$V1, cbind(medium.mu$V2, medium.mu$V3), type = "p", 
        pch = c(1,16), col = c(1,2), main="Medium population size", 
        tck=0.03, ylim = c(0,100),xlab = "Iterations", ylab = "Bit index", 
        cex.lab=2.0, cex.axis=2.0, cex.main=2)
abline(46.76,0, col=4, lty=3, lwd=2)
abline(68.96,0, lty=3)
abline(87.11,0, lty=3, col=4, lwd=2)
legend(0, 100,legend = c("Z","Z star"), pch = c(1,16), col = c(1,2), cex = 1.5)

matplot(large.mu$V1, cbind(large.mu$V2, large.mu$V3), type = "p", 
        pch = c(1,16), col = c(1,2), main="Large population size", 
        tck=0.03, ylim = c(0,100),xlab = "Iterations", ylab = "Bit index", 
        cex.lab=2.0, cex.axis=2.0, cex.main=2)
abline(46.76,0, col=4, lty=3, lwd=2)
abline(68.96,0, lty=3)
abline(87.11,0, lty=3, col=4, lwd=2)
legend(0, 100,legend = c("Z","Z star"), pch = c(1,16), col = c(1,2), cex = 1.5)

par(mfrow=c(1,1))
dev.off()
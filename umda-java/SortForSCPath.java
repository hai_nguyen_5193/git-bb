import java.util.Comparator;

class SortForSCPath implements Comparator<Individual> {
private Evaluator e;

public SortForSCPath(Evaluator e){
        this.e = e;
}

@Override
public int compare(Individual o1, Individual o2){
        int val1 = e.eval(o1);
        int val2 = e.eval(o2);
        if (val2 > val1) {
                return 1;
        } else if (val2 < val1) {
                return -1;
        } else { // equal, break tie randomly
                double r = Math.random();  // [0.0, 1)
                if (r < 0.5) {
                        return 1;
                }else {
                        return -1;
                }
        }
        // if (e.eval(o2) > e.)
        //       return e.eval(o2) - e.eval(o1);
}
}

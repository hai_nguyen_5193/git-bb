#!/bin/bash
#MOAB -j oe
#MOAB -l 'nodes=5:ppn=8,walltime=24:00:00'
#MOAB -A lehrepk01

cd "$PBS_O_WORKDIR"
#echo
#echo "$PBS_JOBID: Job $MOAB_JOBARRAYINDEX of $MOAB_JOBARRAYRANGE in the array"
#
# sleep so that the jobs stay running to look at the queue
#
#echo 'Sleeping for 3 minutes'
#sleep 3m
#    --slf $PBS_NODEFILE \ � -j2 \
#USAGE: ./umda_jump n id r config print_model 
parallel --slf $PBS_NODEFILE \
	 --tag \
	 /gpfs/bb/pxn683/git-bb/umda_jump {1} {2} 3 2 0 \
		::: $(seq 100 100 5000) \
		::: $(seq 1 100) \
	 > bb_jump_r_3_large_mu.log



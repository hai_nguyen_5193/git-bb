/*
 * Implementation of the MIMIC algorithm.
 *
 * Note: We tried to follow the description of the MIMIC algorithm
 * in the origin paper by De Bonet et al. (1997); however, the description was
 * very ambiguous (i.e., undefined conditional probabilities), so we made some
 * further changes to address these problems. One of the most important changes
 * was bounding the probabilities.
 *
 * @author: Hai Nguyen, University of Birmingham
 *
 * This implementation uses the GSL Random Number Generator Library (gsl_rng.h).
 * User needs to install this library before running the code. Assume that
 * you installed libgsl-dev, then the headers should be in /usr/include/gsl/,
 * hence the compiler should be able to locate them using -I/usr/include/gsl
 *
 * USAGE:
 * 1) COMPILE the code using GCC:
 *      gcc -I/usr/include/gsl mimic.c -o mimic -lgsl -lgslcblas -lm
 * 2) RUNNING the compiled code:
 *   ./mimic
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>

//an individual in the population

struct ind {
    int *val; //bit-string as a 1-d array
    int fitness;
    struct ind *next;
};

int mimic(int lmbda, int mu, int n, int opt, int id, int max_iters);
float *entropy(float *en, int mu, int n, int *sum_arr);
float *cond_entropy(float *cond_entropy, int n, int mu, struct ind *head, int *sum_arr);
float calculate_cond_entropy(int x1, int x2, int mu, struct ind *head, int *sum_arr);
float cond_prob(int x1, int x2, int val, int mu, struct ind *head, int *sum_arr);
int index_of_min(float *entropy, int n);
int *reset_array(int *indices, int size);
bool is_value_in_array(int val, int *arr, int size);
int index_of_min_2d(float *cond_entropy, int *chain, int k, int n);
float marginalise(int n, float p);
bool equal(float x, float y);
struct ind *create_ind(int *val, int fitness);
struct ind *add_ind(struct ind *head, struct ind *newind);
struct ind *sort_pop(struct ind *x);
struct ind *find_middle(struct ind *x);
struct ind *merge(struct ind *x, struct ind *y);
int evaluator(int *val, int n);
int *array_sum(int *arr1, int *arr2, int n);
void free_ind(struct ind *p);
void print_ind(int *val, int n);
void print_p(float *p, int n);
void print_model(int iter, int best, int *chain, int n);
int max(int a, int b);

/*
 * MIMIC algorithm
 * Parameters:
 *      lmbda (int) offspring population size
 *      mu    (int) parent population size
 *      n (int) problem instance size
 */
int mimic(int lmbda, int mu, int n, int opt, int id, int max_iters) {

    float *ent = (float *) malloc(sizeof (float)*n); //entropies
    float *cond_ent = (float *) malloc(sizeof (float)*n * n); //conditional entropies
    int chain[n]; // chain-structured model (i.e., a permutation of [n])

    int i, j;

    //float up_border = 1.0-1.0/n;
    //float low_border = 1.0/n;

    // GSL random number generator
    const gsl_rng_type * T;
    gsl_rng * r;
    gsl_rng_env_setup();
    T = gsl_rng_default;
    r = gsl_rng_alloc(T);
    gsl_rng_set(r, (unsigned long) n * id * time(NULL));

    struct ind *head = NULL; //population
    //float m = 0.5;

    // sample the initial population
    for (i = 0; i < lmbda; i++) {
        int *val = (int *) malloc(n * sizeof (int)); // a bitstring
        for (j = 0; j < n; j++) {
            float u = gsl_rng_uniform(r); // a rand in [0,1]
            val[j] = (u <= 0.5) ? 1 : 0;
        }
        int fitness = evaluator(val, n);
        head = add_ind(head, create_ind(val, fitness)); // add to pop
    }

    if (!head) printf("true");
    else printf("false");

    struct ind *curr = NULL;

    printf("population");

    curr = head;
    while (!curr) {
        //int i;
        //for (i=0; i<n; i++){
        //    printf("%d", curr->val[i]);
        //}
        printf("%d", curr->fitness);
        printf("\n");
        curr = curr->next;
    }

    curr = NULL;
    int iters = 0; //iteration count

    //printf("%d \n", iters);

    int *sum_arr = (int *) malloc(sizeof (int)*n);

    while (1) {
        iters++;
        printf("%d \n", iters);

        int count;
        int t;

        head = sort_pop(head); //sort population
        sum_arr = reset_array(sum_arr, n);


        //print out sum_arr
        for (t = 0; t < n; t++) {
            printf("%d ", sum_arr[i]);
        }
        printf("\n");


        // calculate the number of 1s for positions among mu fittest individuals
        curr = head;
        for (t = 0; t < mu; t++) {
            int i;
            for (i = 0; i < n; i++) {
                if (curr->val[i] == 1) sum_arr[i]++;
            }
            curr = curr->next;
        }

        //for (curr=head,t=0; t<mu; curr=curr->next, t++){
        //	sum_arr = array_sum(sum_arr, curr->val, n);
        //}


        //print out sum_arr after function
        for (t = 0; t < n; t++) {
            printf("%d ", sum_arr[i]);
        }
        printf("\n");


        int k = 0; //newly-added node

        // choosing the root
        ent = entropy(ent, mu, n, sum_arr);
        chain[k] = index_of_min(ent, n);

        //print out root
        printf("root %d \n", chain[k]);

        //choosing the rest of chain-like model
        cond_ent = cond_entropy(cond_ent, n, mu, head, sum_arr);
        for (t = k + 1; t < n; t++) {
            chain[t] = index_of_min_2d(cond_ent, chain, t - 1, n);
        }

        //print out whole model
        printf("complete model: ");
        for (t = 0; t < n; t++) {
            printf("%d ", chain[t]);
        }
        printf("\n");

        print_model(iters, head->fitness, chain, n);

        if (head->fitness == opt) { //terminate?
            printf("optimal solution found: %d ", head->fitness);
            break;
        } else if (iters > max_iters) {
            printf("iters exceeded %d ", max_iters);
            break;
        }

        //sample new population, starting from the root
        curr = head;
        while (curr != NULL) {
            for (i = 0; i < n; i++) {
                if (i == 0) { //root
                    int indx = chain[0];
                    float prob = (float) sum_arr[indx] / mu;
                    float u = gsl_rng_uniform(r); // a rand
                    curr->val[indx] = (u <= marginalise(n, prob)) ? 1 : 0;
                } else {
                    int x1 = chain[i];
                    int x2 = chain[i - 1]; //newly-added node
                    float prob = cond_prob(x1, x2, curr->val[x2], mu, head, sum_arr); //x1 conditioned on x2
                    float u = gsl_rng_uniform(r);
                    curr->val[x1] = (u <= marginalise(n, prob)) ? 1 : 0;
                }
            }
            curr->fitness = evaluator(curr->val, n);
            curr = curr->next;
        }
    }
    free(ent);
    free(cond_ent);
    free(chain);
    free(sum_arr);
    free_ind(head);
    return lmbda*iters;
}

//univariate entropy

float *entropy(float *en, int mu, int n, int *sum_arr) {
    int indx = 0;
    for (; indx < n; indx++) {
        if (sum_arr[indx] == 0 || sum_arr[indx] == mu) {
            en[indx] = 0;
        } else {
            float p = (float) sum_arr[indx] / mu;
            en[indx] = (float) -(p * log(p) + (1 - p) * log(1 - p));
        }
    }
    return en;
}

//return matrix of cond. entropy

float *cond_entropy(float *cond_entropy, int n, int mu, struct ind *head, int *sum_arr) {
    int row, col;
    for (row = 0; row < n; row++) { //avoid col==row, same variable.
        for (col = 0; col < row; col++) {
            cond_entropy[row * n + col] = calculate_cond_entropy(row, col, mu, head, sum_arr);
        }
        for (col = row + 1; col < n; col++) {
            cond_entropy[row * n + col] = calculate_cond_entropy(row, col, mu, head, sum_arr);
        }
    }
    return cond_entropy;
}


//return cond. entropy for pair of varibles

float calculate_cond_entropy(int x1, int x2, int mu, struct ind *head, int *sum_arr) {
    int elems;
    int ones_0 = 0;
    int ones_1 = 0;
    float h_0; //entr. when x2=0
    float h_1; //entr. when x2=1
    float h;
    struct ind *curr = head;
    for (elems = 0; elems < mu; elems++, curr = curr->next) {
        if ((curr->val[x2] == 0) && (curr->val[x1] == 1)) ones_0++;
        if ((curr->val[x2] == 1) && (curr->val[x1] == 1)) ones_1++;
    }

    float p_1, p_0;
    if (sum_arr[x2] == mu) {
        p_1 = (float) ones_1 / mu;
        if ((p_1 > 1.0 - 1.0 / mu) || (p_1 < 1.0 / mu)) h = 0;
        else h = -(p_1) * log(p_1) - (1 - p_1) * log(1 - p_1);
    } else if (sum_arr[x2] == 0) {
        p_0 = (float) ones_0 / mu;
        if ((p_0 > 1.0 - 1.0 / mu) || (p_0 < 1.0 / mu)) h = 0;
        else h = -p_0 * log(p_0) - (1 - p_0) * log(1 - p_0);
    } else {
        //cond. entropy when x2=1
        p_1 = (float) ones_1 / sum_arr[x2];
        if ((p_1 > 1.0 - 1.0 / sum_arr[x2]) || (p_1 < 1.0 / sum_arr[x2])) h_1 = 0;
        else h_1 = -(p_1) * log(p_1) - (1 - p_1) * log(1 - p_1);

        //cond. entropy when x2=0
        p_0 = (float) ones_0 / (mu - sum_arr[x2]);
        if ((p_0 > 1.0 - 1.0 / (mu - sum_arr[x2])) || (p_0 < 1.0 / (mu - sum_arr[x2]))) h_0 = 0;
        else h_0 = -p_0 * log(p_0) - (1 - p_0) * log(1 - p_0);

        h = h_1 * ((float) sum_arr[x2] / mu) + h_0 * (1 - ((float) sum_arr[x2] / mu));
    }
    return h;
}

//conditional probabilities, used for sampling

float cond_prob(int x1, int x2, int val, int mu, struct ind *head, int *sum_arr) {
    struct ind *curr = head;
    int elems = 0;
    int ones = 0;
    for (; elems < mu; elems++, curr = curr->next) {
        if ((curr->val[x2] == val) && (curr->val[x1] == 1)) ones++;
    }
    if (val == 1)
        return (float) ones / sum_arr[x2];
    else
        return (float) ones / (mu - sum_arr[x2]);
}


// return indices of max

int index_of_min(float *entropy, int n) {
    int i;
    int *indices = (int *) malloc(n * sizeof (int));
    indices = reset_array(indices, n);
    int indx = 0;
    indices[indx] = 1;
    for (i = 1; i < n; i++) {
        if (equal(entropy[i], entropy[indx])) { //check for equal FIRST
            indices[i] = 1;
        } else if (entropy[i] < entropy[indx]) {
            indices = reset_array(indices, n);
            indices[i] = 1;
            indx = i;
        }
    }
    int size = 0;
    for (i = 0; i < n; i++) {
        if (indices[i] == 1) size++;
    }
    int *res = (int *) malloc(sizeof (int)*size);
    int j = 0;
    for (i = 0; i < n; i++) {
        if (indices[i] == 1) {
            res[j] = i;
            j++;
        }
    }
    int z;
    if (size == 1) z = res[0];
    else if (size != 0) {
        srand(time(NULL));
        z = res[rand() % size];
    }

    //release memory after use
    free(indices);
    free(res);
    return z;
}

//check if two floating points equal

bool equal(float x, float y) {
    float tolerent = 0.0001;
    if ((x - y < tolerent && x > y) || (y - x < tolerent && y > x)) {
        return true;
    }
    return false;
}


//reset all alems in array to zeros

int *reset_array(int *indices, int size) {
    int i = 0;
    for (; i < size; i++) {
        indices[i] = 0;
    }
    return indices;
}


//check if value in an array

bool is_value_in_array(int val, int *arr, int size) {
    int i;
    for (i = 0; i < size; i++) {
        if (arr[i] == val)
            return true;
    }
    return false;
}


//index of max in 2d array

int index_of_min_2d(float *cond_entropy, int *chain, int k, int n) {
    int i;
    int *indices = (int *) malloc(n * sizeof (int));
    indices = reset_array(indices, n);
    int indx;

    //initialising
    for (i = 0; i < n; i++) {
        if (is_value_in_array(i, chain, k + 1) == false)
            indx = i;
        break;
    }
    indices[indx] = 1;
    for (i = indx + 1; i < n; i++) {
        if (is_value_in_array(i, chain, k + 1) == false) {
            if (equal(cond_entropy[i * n + chain[k]], cond_entropy[indx * n + chain[k]]) == true) { //check for equal FIRST
                indices[i] = 1;
            } else if (cond_entropy[i * n + chain[k]] < cond_entropy[indx * n + chain[k]]) {
                indices = reset_array(indices, n);
                indx = i;
                indices[i] = 1;
            }
        }
    }
    int size = 0;
    for (i = 0; i < n; i++) {
        if (indices[i] == 1) size++;
    }
    int *res = (int *) malloc(sizeof (int)*size);
    int j = 0;
    for (i = 0; i < n; i++) {
        if (indices[i] == 1) {
            res[j] = i;
            j++;
        }
    }
    int z;
    if (size == 1) z = res[0];
    else {
        srand(time(NULL));
        z = res[rand() % size];
    }

    //release memory after use
    free(indices);
    free(res);

    return z;
}


//marginalise p within [1/n,1-1/n]

float marginalise(int n, float p) {
    float up_border = 1.0 - 1.0 / n;
    float low_border = 1.0 / n;
    if (p < low_border) p = low_border;
    else if (p > up_border) p = up_border;
    return p;
}


//print an individual

void print_ind(int *val, int n) {
    int i;
    for (i = 0; i < n; i++) {
        printf("%d ", val[i]);
    }
    printf("\n");
}


//print the current probability model

void print_p(float *p, int n) {
    int i;
    printf("Prob. model:\n");
    for (i = 0; i < n; i++) {
        printf("%.2f ", p[i]);
    }
    printf("\n");
}


//print the current model

void print_model(int iter, int best, int *chain, int n) {
    int i = 0;
    printf("%d \t %d \t", iter, best);
    for (; i < n; i++) {
        printf("%d", chain[i]);
    }
    printf("\n");
}


//evaluate fitness value (DLB)

int evaluator(int *val, int n) {
    int width = 2;
    int i, j; //index
    int fitness = 0;
    for (i = 0; i < n; i = i + width) { //only one loop
        if (val[i] == 1 && val[i + 1] == 1) { //11
            fitness = fitness + 2;
        } else if ((val[i] == 0 && val[i + 1] == 1) || (val[i] == 1 && val[i + 1] == 0)) { //01 and 10
            fitness = fitness;
            break;
        } else { //00
            fitness = fitness + 1;
            break;
        }
    }
    return fitness;
}

int *array_sum(int *arr1, int *arr2, int n) {
    int i;
    for (i = 0; i < n; i++) {
        arr1[i] = arr1[i] + arr2[i];
    }
    return arr1;
}

//create a dictionary

struct ind *create_ind(int *val, int fitness) {
    struct ind *p = (struct ind *) malloc(sizeof (struct ind));
    if (p == NULL) {
        return NULL;
    }
    p->val = val;
    p->fitness = fitness;
    p->next = NULL;
    return p;
}

//add a new dict into the current dict

struct ind *add_ind(struct ind *head, struct ind *new) {
    if (head == NULL) {
        return new;
    }
    if (new != NULL) {
        new->next = head;
        head = new;
    }
    return head;
}

//free the memory

void free_ind(struct ind *head) {
    struct ind *tmp;
    while (head != NULL) {
        tmp = head;
        head = head->next;
        free(tmp->val);
        free(tmp);
    }
}

//sort a dictionary

struct ind *sort_pop(struct ind *x) {

    struct ind *y = NULL;
    struct ind *m = NULL;

    if (x == NULL || x->next == NULL) {
        return x;
    }
    m = find_middle(x);
    y = m->next;
    m->next = NULL;
    return merge(sort_pop(x), sort_pop(y));
}

//find the middle elem in a dictionary

struct ind *find_middle(struct ind *x) {

    struct ind *slow = x;
    struct ind *fast = x;

    while (fast->next != NULL && fast->next->next != NULL) {
        slow = slow->next;
        fast = fast->next->next;
    }
    return slow;
}

//merge two dicts

struct ind *merge(struct ind *x, struct ind *y) {

    struct ind *tmp = NULL;
    struct ind *head = NULL;
    struct ind *curr = NULL;

    if (x == NULL) {
        head = y;
    } else if (y == NULL) {
        head = x;
    } else {
        while (x != NULL && y != NULL) {

            // Swap x and y if x is not largest.
            if (x->fitness < y->fitness) {
                tmp = y;
                y = x;
                x = tmp;
            }

            if (head == NULL) { // First element?
                head = x;
                curr = x;
            } else {
                curr->next = x;
                curr = curr->next;
            }
            x = x->next;
        }

        // Either x or y is empty.
        if (x != NULL) {
            curr->next = x;
        } else if (y != NULL) {
            curr->next = y;
        }
    }
    return head;
}

//find the max between two ints

int max(int a, int b) {
    return (a >= b) ? a : b;
}

int main(int argc, char **argv) {

    if (argc != 6) {
        printf("USAGE: %s n pop id print_model max_iters", argv[0]);
        exit(1);
    }

    int n = atoi(argv[1]); //problem size n
    int pop = atoi(argv[2]); //population
    int id = atoi(argv[3]); //job id
    int model_print = atoi(argv[4]);
    int max_iters = atoi(argv[5]); //threshold in iters
    printf("n=%d, pop=%d, print_model=%d, max_iters=%d", n, pop, model_print, max_iters);

    int mu;
    float log2n = log10(n) / log10(2);

    if (pop == 1) { //small mu
        mu = (int) sqrt(n); //sqrt(n)
    } else if (pop == 2) { //medium mu
        mu = (int) sqrt(n) * log2n; //sqrt(n)*log2(n)
    } else if (pop == 3) { //large pop
        mu = (int) n;
    }

    int lmbda = (int) 10 * mu; //lambda = 10mu

    int opt = n;

    int runtime = mimic(lmbda, mu, n, opt, id, max_iters);

    if (model_print == 0) {
        printf("%d \n", runtime);
    }

    return (0);
}
